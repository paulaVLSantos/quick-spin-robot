package quick_spin;

import java.awt.Color;
import robocode.AdvancedRobot;
import robocode.HitByBulletEvent;
import robocode.HitRobotEvent;
import robocode.HitWallEvent;
import robocode.ScannedRobotEvent;

public class QuickSpin extends AdvancedRobot {

	public void run() {
		setColors(Color.pink, Color.white, Color.pink);
		setScanColor(Color.white);
		setBulletColor(Color.white);

		while (true) {

			if (isNearWall()) {
				setBack(100);
			} else {
				setTurnRight(800);
				ahead(800);

			}
			execute();

		}

	}

	public void onScannedRobot(ScannedRobotEvent enemy) {
		this.shoot(enemy.getDistance());

	}

	public void onHitByBullet(HitByBulletEvent enemy) {
		toAim(enemy.getBearing());
		this.shoot(enemy.getBearing());

	}

	public void onHitRobot(HitRobotEvent enemy) {
		toAim(enemy.getBearing());
		fire(3);

	}

	public void onHitWall(HitWallEvent parede) {
		setBack(100);

	}

	public boolean isNearWall() {
		return (getX() < 50 || getX() > getBattleFieldWidth() - 50 ||

				getY() < 50 || getY() > getBattleFieldHeight() - 50);
	}

	public void shoot(double distancia) {
		if (distancia >= 200 || distancia <= 100 || getEnergy() < 20) {
			fire(1);

		} else if (distancia <= 50) {
			fire(2);

		} else
			fire(3);
	}

	public void toAim(double inimigo) {
		double valor = getHeading() + inimigo - getGunHeading();
		if (!(valor > -180 && valor <= 180)) {
			while (valor <= -180) {
				valor += 360;
			}
			while (valor > 180) {
				valor -= 360;
			}
		}
		turnGunRight(valor);
	}

}
