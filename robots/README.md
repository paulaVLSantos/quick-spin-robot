#Rob� Application
##Rob� criado no estilo robocode utilizando linguagem de programa��o Java.
Foi um desafio interessante e estimulante que me proporcionou superar meus objetivos e experimentar novas coisas em 
um ambiente java.
Configurar o robocode para integr�-lo ao eclipse foi novo pra mim. Al�m disso, pensar do ponto de vista da intelig�ncia
artificial n�o era algo que eu fazia com frequencia, e criar esse projeto me fez "pensar fora da caixa".
Meu rob� faz movimentos r�pidos de esquiva e limita a pot�ncia da bala em diferentes dist�ncias. 
Acredito que sejam movimentos essenciais para a efici�ncia dele.
Est� sendo muito interessante e motivador participar do processo seletivo.